package com.loginregisterrecovery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity( securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	public void configureAuth(AuthenticationManagerBuilder auth) throws Exception{
		auth
		.userDetailsService(userService)
		.passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http)throws Exception{

		http.sessionManagement().maximumSessions(10).sessionRegistry(sessionRegistry());

		http
		.authorizeRequests()
		.mvcMatchers("/css/**").permitAll()
		.mvcMatchers("/images/**").permitAll()
		.mvcMatchers("/js/**").permitAll()
		.mvcMatchers("/dist/**").permitAll()
		.mvcMatchers("/DataTables/**").permitAll()
		.mvcMatchers("/login").permitAll()
		.mvcMatchers("/forgot").permitAll()
		.mvcMatchers("/register").permitAll()
		.mvcMatchers("/user/changePassword").permitAll()
		.mvcMatchers("/logout").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login")
		.usernameParameter("usernameparam")
		.permitAll()
		.and()
		.logout()
		.logoutSuccessUrl("/login")
		.permitAll()
		.and().exceptionHandling().accessDeniedPage("/login");
	}
	
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}

	@Bean
	public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
		return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
	}
}
