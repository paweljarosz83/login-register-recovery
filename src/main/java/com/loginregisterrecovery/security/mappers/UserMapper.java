package com.loginregisterrecovery.security.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.loginregisterrecovery.security.model.Role;
import com.loginregisterrecovery.security.model.User;
import com.loginregisterrecovery.security.model.UserDTO;

@Component
public class UserMapper {
	
	private static final Role ROLE_ADMIN = new Role("ROLE_ADMIN");
	private static final Role ROLE_USER = new Role("ROLE_USER");
	private static final Role ROLE_MODERATOR = new Role("ROLE_MODERATOR");
	
	public UserDTO mapToDto(User entity){
		
		UserDTO dto = new UserDTO();
		
		dto.setId(entity.getId());
		dto.setUsername((entity.getUsername()));
		dto.setEmail((entity.getEmail()==null)?"":entity.getEmail());
		dto.setMobile(entity.getMobile());
		
		dto.setName(entity.getName());
		dto.setSurname(entity.getSurname());
		dto.setLocation(entity.getLocation());
		dto.setPosition(entity.getPosition());
		
		if(entity.getRoles().contains(ROLE_ADMIN)){
			dto.setAdmin(true);
		}
		
		if(entity.getRoles().contains(ROLE_USER)){
			dto.setUser(true);
		}
		
		if(entity.getRoles().contains(ROLE_MODERATOR)){
			dto.setModerator(true);
		}
		return dto;
	}
	
	public User mapToEntity(UserDTO dto){
		
		User entity = new User();
		entity.setId(dto.getId());
		entity.setUsername(dto.getUsername());
		entity.setEmail(dto.getEmail());
		entity.setMobile(dto.getMobile());
		entity.setName(dto.getName());
		entity.setSurname(dto.getSurname());
		entity.setLocation(dto.getLocation());
		entity.setPosition(dto.getPosition());
		return entity;
	}
	
	public List<UserDTO>map(List<User> users){
		return users.stream().map(u->mapToDto(u)).collect(Collectors.toList());
	}
}







