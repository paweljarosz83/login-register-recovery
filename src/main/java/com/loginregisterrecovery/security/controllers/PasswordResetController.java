package com.loginregisterrecovery.security.controllers;

import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.loginregisterrecovery.security.model.EmailDTO4Reset;
import com.loginregisterrecovery.security.model.PasswordResetDTO;
import com.loginregisterrecovery.security.model.User;
import com.loginregisterrecovery.security.model.token.PasswordResetToken;
import com.loginregisterrecovery.security.services.PasswordResetService;
import com.loginregisterrecovery.security.services.UserService;
import com.loginregisterrecovery.security.services.mail.MailService;
import com.loginregisterrecovery.security.services.token.PasswordResetTokenService;
import com.loginregisterrecovery.security.services.validators.EmailResetDTOValidator;
import com.loginregisterrecovery.security.services.validators.PasswordResetDTOValidator;

@Controller
public class PasswordResetController {


	@Autowired
	private MailService mailService;
	
	@Autowired
	private MessageSource messages;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EmailResetDTOValidator validator;

	@Autowired
	private PasswordResetService passwordResetService;
	
	@Autowired
	private PasswordResetTokenService passwordResetTokenService;
	
	@Autowired
	private PasswordResetDTOValidator passwordResetDTOValidator;
	

	
	/**
	 * Display forgot password page. Provide empty dto for email
	 * 
	 * @param emailResetDTO dto to grab an email
	 * @return
	 */
	@RequestMapping(value = "/forgot", method = RequestMethod.GET)
	public String forgot(EmailDTO4Reset emailDTO4Reset){
		return "auth/forgot";
	}

	@RequestMapping(value = "/forgot", method = RequestMethod.POST)
	public String resetPassword(@Valid EmailDTO4Reset emailDTO4Reset, BindingResult bindingResult,final RedirectAttributes redirectAttributes,final HttpServletRequest request) {

		validator.validate(emailDTO4Reset, bindingResult);

		if (bindingResult.hasErrors()) {
			return "auth/forgot";
		}

		redirectAttributes.addFlashAttribute("message", "Recovery was link sent. Check your email.");		
		User user = userService.findByEmail(emailDTO4Reset.getEmail());
		String tokenString = UUID.randomUUID().toString();
		PasswordResetToken token = new PasswordResetToken(tokenString, user);
		passwordResetTokenService.save(token);
		String url = createResetPasswordUrl(request,tokenString,user);
		sendPasswordRecoveryEmail(url,user.getEmail());

		return "redirect:/forgot";
	}
	
	private String createResetPasswordUrl(HttpServletRequest request, String tokenString, User user) {
		return "http://localhost:8075/user/changePassword?id=" + user.getId() + "&token=" + tokenString;
	}

	private void sendPasswordRecoveryEmail(String url,String email) {
		mailService.sendPasswordRecoveryEmail(url,email);
	}
	
	/**
	 * Accepts a link from an email. Validates a token and an id. 
	 * Showes and error on failure or pass change from on success 
	 * 
	 * @param locale			
	 * @param model 			model to add a message to
	 * @param id				user id from recovery link
	 * @param token 			token from recovery link
	 * @param passwordResetDTO
	 * @return
	 */
	@RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
	public String showChangePasswordPage(Locale locale, Model model,  @RequestParam("id") long id, @RequestParam("token") String token,PasswordResetDTO passwordResetDTO) {
		
		passwordResetDTO.setUserId(id);
		String result = passwordResetTokenService.validatePasswordResetToken(id, token);

		if (result != null) {
			model.addAttribute("message", messages.getMessage("password.reset.message." + result, null, locale));
			return "auth/resetPasswordFailed";
		}else{
			return "auth/resetPassword";
		}
	}

	/**
	 * Password reset from submit method.
	 * @param passwordResetDTO
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	
	@RequestMapping(value="/resetPassword", method = RequestMethod.POST)
	public String resetPasswordSave(@Valid PasswordResetDTO passwordResetDTO, BindingResult bindingResult,Model model,RedirectAttributes redirectAttrs){
		
		passwordResetDTOValidator.validate(passwordResetDTO, bindingResult);

		if (bindingResult.hasErrors()) {
			return "auth/resetPassword";
		}else{
			passwordResetService.setNewPassword(passwordResetDTO);
			model.addAttribute("message","Done!");
			redirectAttrs.addAttribute("message","Done!");
			return "auth/login";
			
		}
	}
}
