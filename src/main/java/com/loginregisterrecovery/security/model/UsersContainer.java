package com.loginregisterrecovery.security.model;

import java.util.ArrayList;
import java.util.List;

public class UsersContainer {
	
	private List<UserDTO> data = new ArrayList<>();

	public List<UserDTO> getData() {
		return data;
	}

	public void setData(List<UserDTO> data) {
		this.data = data;
	}
}
