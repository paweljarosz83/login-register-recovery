package com.loginregisterrecovery.security.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordResetDTO {
	

	private Long userId;

	@NotNull
	@Size(min = 8, max = 20,message="Password should have a minimum 8 characters")
	private String password;
	
	@NotNull
	@Size(min = 8, max = 20,message="Password should have a minimum 8 characters")
	private String passwordRepeated;
	
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeated() {
		return passwordRepeated;
	}

	public void setPasswordRepeated(String passwordRepeated) {
		this.passwordRepeated = passwordRepeated;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
