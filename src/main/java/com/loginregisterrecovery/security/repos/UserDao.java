package com.loginregisterrecovery.security.repos;

import java.util.List;

import com.loginregisterrecovery.daos.GenericDao;
import com.loginregisterrecovery.security.model.User;
import com.loginregisterrecovery.security.model.UserDTO;



public interface UserDao extends GenericDao<User,Long>{
	
	User findUserById(Long id);
	
	List<UserDTO> findAllDTOs();
	
	List<User> findAll();

	String saveUser(UserDTO dto);

	void deleteUser(Long userId);



}
