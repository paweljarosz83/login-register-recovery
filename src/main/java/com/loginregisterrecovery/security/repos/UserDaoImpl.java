package com.loginregisterrecovery.security.repos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.loginregisterrecovery.daos.GenericDaoImpl;
import com.loginregisterrecovery.security.mappers.UserMapper;
import com.loginregisterrecovery.security.model.Role;
import com.loginregisterrecovery.security.model.User;
import com.loginregisterrecovery.security.model.UserDTO;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User,Long> implements UserDao{

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UserMapper mapper;

	@Autowired
	private UserRepository repo;

	@Autowired
	private RoleRepository roleRepo;





	public UserDTO findUserDTOById(Long id) {
		return mapper.mapToDto(repo.findById(id).get());
	}

	@Override
	public User findUserById(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<UserDTO> findAllDTOs() {

		List<User>users =  new ArrayList<>();
		repo.findAll(Sort.by(Sort.Direction.DESC, "id")).forEach(users::add);
		return mapper.map(users);
	}

	@Override
	public List<User> findAll() {

		List<User>users =  new ArrayList<>();
		repo.findAll(Sort.by(Sort.Direction.DESC, "id")).forEach(users::add);
		return users;
	}

	public String saveUser(UserDTO dto) {

		System.out.println("dto.getId() "+dto.getId());
		
		if(dto.getId()==null){
			return saveNewUser(dto);
		}else{
			return updateUser(dto);
		}
	}

	private String saveNewUser(UserDTO dto) {
System.out.println("save new");
		User user = mapper.mapToEntity(dto);
		updateRoles(dto, user);

		if(dto.getPassword()==null){
			user.setPassword(new BCryptPasswordEncoder().encode("abc123"));
		}else{
			user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
		}
		repo.save(user);
		return "zapisany";
	}

	private void updateRoles(UserDTO dto, User user) {

		if(dto.isAdmin()){
			Role role = roleRepo.findByRole("ROLE_ADMIN");
			role.getUsers().add(user);
			user.getRoles().add(role);
		}

		if(dto.isUser()){
			Role role = roleRepo.findByRole("ROLE_USER");
			role.getUsers().add(user);
			user.getRoles().add(role);
		}
		
		if(dto.isModerator()){
			Role role = roleRepo.findByRole("ROLE_MODERATOR");
			role.getUsers().add(user);
			user.getRoles().add(role);
		}
	}

	private String updateUser(UserDTO dto) {
		System.out.println("update user");
		User user = repo.findById(dto.getId()).get();
		updateFields(user,dto);
		user.setRoles(new HashSet<Role>());
		updateRoles(dto, user);
		em.merge(user);
		return "saved";
	}

	private void updateFields(User entity,UserDTO dto){

		System.out.println("dto.getEmail()dd "+dto.getEmail());
		
		entity.setId(dto.getId());
		entity.setUsername(dto.getUsername());
		entity.setEmail(dto.getEmail());
		entity.setMobile(dto.getMobile());
		entity.setName(dto.getName());
		entity.setSurname(dto.getSurname());
		entity.setLocation(dto.getLocation());
		entity.setPosition(dto.getPosition());
	}

	@Override
	public void deleteUser(Long userId) {
		
		User u = repo.findById(userId).get();
		u.getRoles().clear();
		repo.delete(u);
	}



}
