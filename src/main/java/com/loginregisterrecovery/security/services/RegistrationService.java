package com.loginregisterrecovery.security.services;

import com.loginregisterrecovery.security.model.UserDTO4Registration;

public interface RegistrationService {

	void register(UserDTO4Registration dto);
}
