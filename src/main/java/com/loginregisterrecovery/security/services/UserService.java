package com.loginregisterrecovery.security.services;

import com.loginregisterrecovery.security.model.User;

public interface UserService {

	public User findByEmail(String email);
	
	public User findByUsername(String username);
	
	public User findById(Long id);
	
	public void update(User user);



	public User getLoggedUser();
	
	
	
}
