package com.loginregisterrecovery.security.services;

import javax.validation.Valid;

import com.loginregisterrecovery.security.model.PasswordResetDTO;

public interface PasswordResetService {

	void setNewPassword(@Valid PasswordResetDTO passwordResetDTO);

}
