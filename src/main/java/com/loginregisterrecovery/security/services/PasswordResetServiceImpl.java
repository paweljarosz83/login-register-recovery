package com.loginregisterrecovery.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.loginregisterrecovery.security.model.PasswordResetDTO;
import com.loginregisterrecovery.security.model.User;
import com.loginregisterrecovery.security.services.token.PasswordResetTokenService;

@Service
public class PasswordResetServiceImpl implements PasswordResetService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordResetTokenService passwordResetTokenService;
	
	@Override
	public void setNewPassword(PasswordResetDTO dto) {
		
		User user = userService.findById(dto.getUserId());
		
		if(user!=null){
			user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
			userService.update(user);
			passwordResetTokenService.invalidateOtherTokensForUser(dto.getUserId());
		}
	}
}
