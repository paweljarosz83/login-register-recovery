package com.loginregisterrecovery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.loginregisterrecovery.security.model.UserDTO;
import com.loginregisterrecovery.security.model.UsersContainer;
import com.loginregisterrecovery.security.repos.UserDaoImpl;

@Controller
public class UsersController {
	
	
	@Autowired
	private UserDaoImpl dao;
	

	@RequestMapping("/")
	public String root(){
		return "main/index";
	}
	
	@RequestMapping("/index")
	public String index(){
		return "main/index";
	}
	
	@RequestMapping("/getUsers")
	@ResponseBody
	public UsersContainer findAll(){
		UsersContainer c = new UsersContainer();
		c.setData(dao.findAllDTOs());
		return c;
	}
	
	@RequestMapping("/newUser")
	public String newUser(Model model){
		model.addAttribute("userDTO",new UserDTO());
		return "main/userForm";
	}

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@RequestMapping(value="/saveUser",method=RequestMethod.POST)
	@ResponseBody
	public String saveUser(UserDTO userDTO){
		dao.saveUser(userDTO);
		return "saved";
	}

	@RequestMapping("/user/{id}")
	public String showUserDetails(@PathVariable Long id,Model model){
		model.addAttribute("userDTO",dao.findUserDTOById(id));
		return "main/userForm";
	}
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@RequestMapping(value="/deleteUser",method=RequestMethod.POST)
	@ResponseBody
	public String deleteUser(@RequestParam Long userId,Model model){
		dao.deleteUser(userId);
		return "deleted";
	}
}



















