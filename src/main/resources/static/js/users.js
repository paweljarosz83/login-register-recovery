$(document).ready(function() {


	$("#userCheckbox").attr("checked",'checked');
	$("#userCheckbox").attr("disabled",'disabled');
	$("#userLoader").hide();
	$("#successImg").hide();


	var table = $("#usersTable").DataTable({
		language: {
			processing:     "Processing",
			search:         "Search",
			lengthMenu:     "Show _MENU_ results on page",
			info:           "Page  _PAGE_ of _PAGES_",
			infoPostFix:    "",
			infoEmpty:      "No match",
			loadingRecords: "Loading",
			infoFiltered:   "",
			zeroRecords:    "No users",
			emptyTable:     "No users",
			paginate: {
				first:      "1st",
				previous:   "2nd",
				next:       "next",
				last:       "last"
			}
		},
		'iDisplayLength': 50,
		'order': [[ 0, 'desc' ]],
		'ajax':'/getUsers',
		'dataSrc':'data',
		'columns':[
		           { "data": "id" },
		           { "data": "name" },
		           { "data": "surname" },
		           { "data": "username" },
		           { "data": "location" },
		           { "data": "email" },
		           { "data": "mobile" },

		           { "data": "id",
		        	   "render": function(data,type,row,meta){
		        		   return "<a href='/user/"+data+"'>Open</a>";
		        	   }
		           },

		           ]
	});
	
	
	$("#saveUserButton").click(function(){

		var username = $("#usernameField").val();
		var email = $("#emailField").val();

		if(username ==="" || email===""){
			$("#usernameError").text("Uzupełnij nazwę użytkownika (login)");
			$("#emailError").text("Uzupełnij adres email");
			return;
		}else{
			$("#usernameError").text("");
			$("#emailError").text("");
			submitForm();
		}
	});

	function submitForm(){

		let userAsJson = {
				'id':$("#userIdHiddenField").val(),
				'name':$("#nameField").val(),
				'surname':$("#surnameField").val(),
				'username':$("#usernameField").val(),
				'email':$("#emailField").val(),
				'mobile':$("#mobileField").val(),
				'location':$("#locationField").val(),
				
				'admin':$("#isAdminCheckbox").is(':checked'),
				'user':$("#isUserCheckbox").is(':checked'),
				'moderator':$("#isModeratorCheckbox").is(':checked')
		}
		
		var token = $('#_csrf').attr('content');
		var header = $('#_csrf_header').attr('content');
	
		$.ajax({
			url:'/saveUser',
			method:'POST',
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			data:userAsJson,
			success:function(data){
				$("#successImg").show();
				
				 setTimeout( function(){
					 $("#successImg").hide();
				 }, 2000 );
			},
			error:function(data){
				
			}
		})
	}

	$("#deleteUserButton").click(function(){

		var userId = $("#userIdHiddenField").val();

		var token = $('#_csrf').attr('content');
		var header = $('#_csrf_header').attr('content');

		$.ajax({
			url:'/deleteUser',
			data:{
				'userId':userId
			},
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			method:'POST',
			success:function(data){

				setTimeout( function(){
					window.open('/index','_self');
				}, 1000 );
			}
		})
	});
});


























